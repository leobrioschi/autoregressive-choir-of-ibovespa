# **AutoRegressive Choir of Ibovespa**
*(Meninas AutoRegressoras do [Ibovespa](https://finance.yahoo.com/quote/%5EBVSP/))* - **[Go To Pages](https://leobrioschi.gitlab.io/autoregressive-choir-of-ibovespa/)** 

This is a simple trading strategy based on building AR models for each stock and then running the forecast daily to pick the best 10 returns to buy next day. There is no stop loss but in few cases the model won't buy anything **if forecasts are lower than 1.7%**.

The idea is simple, buy at **Open** and sell at **Close**, yes, it is intraday everyday. At least by theory it should always be easy to sell at **Close** price, since recent *function control for stocks sellouts research indicates just that.*

**Trading costs were not considered,** it is more an excercise than anything.

Thank you for reading and taking a look, reffer to the **Comments** and the **Out of sample** sections on the notebook for more information

## Contributing
Any pointers and contribution are welcome! Though, this strategy shouldn't yield much return in the long run, and might need lots of supervision. Just fork, work and send a merge request!

## Authors and acknowledgment
I am *Leonardo Brioschi*, a Ph.D. Student in Accounting/Finance. Please visit **https://leobrioschi.gitlab.io**

I would also like to thank [*Felipe Scolari Barbosa*](https://www.linkedin.com/in/felipe-scolari-barbosa-284220237/) for the first input and contribution!

## License
This follows a MIT License, read more at *LICENSE*.

## Project status
I wish to organize a small analysis on a static website (*hence the saved graphics*), but for now it is on hold.
